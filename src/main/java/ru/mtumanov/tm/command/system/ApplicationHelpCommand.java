package ru.mtumanov.tm.command.system;

import java.util.Collection;

import ru.mtumanov.tm.command.AbstractCommand;

public class ApplicationHelpCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-h";
    }

    @Override
    public String getDescription() {
        return "Show list arguments";
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) System.out.println(command);
    }
    
}
