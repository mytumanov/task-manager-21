package ru.mtumanov.tm.command.system;

import java.util.Collection;

import ru.mtumanov.tm.command.AbstractCommand;

public class ArgumentListCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-arg";
    }

    @Override
    public String getDescription() {
        return "Show argument list";
    }

    @Override
    public String getName() {
        return "arguments";
    }

    @Override
    public void execute() {
        System.out.println("[Arguments]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }
    
}
