package ru.mtumanov.tm.exception.user;

public class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("ERROR! You are not logged in. Please log in and try again!");
    }
    
}
