package ru.mtumanov.tm.api.repository;

import java.util.Comparator;
import java.util.List;

import ru.mtumanov.tm.enumerated.Sort;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    M add(String userId, M model);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    List<M> findAll(String userId, Sort sort);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    M remove(String userId, M model) throws AbstractEntityNotFoundException;

    M removeById(String userId, String id) throws AbstractEntityNotFoundException;

    M removeByIndex(String userId, Integer index) throws AbstractEntityNotFoundException;

    void clear(String userId);

    int getSize(String userId);

    boolean existById(String userId, String id);
    
}
