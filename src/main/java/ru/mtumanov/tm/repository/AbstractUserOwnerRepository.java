package ru.mtumanov.tm.repository;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import ru.mtumanov.tm.api.repository.IUserOwnedRepository;
import ru.mtumanov.tm.enumerated.Sort;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.exception.entity.EntityNotFoundException;
import ru.mtumanov.tm.model.AbstractUserOwnedModel;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    @Override
    public M add(final String userId, final M model) {
        if (userId == null) return null;
        models.add(model);
        return model;
    }

    @Override
    public void clear(final String userId) {
        final List<M> modelList = findAll(userId);
        models.removeAll(modelList);
    }

    @Override
    public boolean existById(final String userId, final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null)
            return Collections.emptyList();
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) {
        return (List<M>) findAll(userId).stream()
                .sorted(sort.getComparator())
                .collect(Collectors.toList());
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        return models.stream()
                .filter(m -> id.equals(m.getId()) && userId.equals(m.getUserId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public int getSize(final String userId) {
        return (int) models.stream().count();
    }

    @Override
    public M remove(final String userId, final M model) throws AbstractEntityNotFoundException {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Override
    public M removeById(final String userId, final String id) throws AbstractEntityNotFoundException {
        if (userId == null || id == null) return null;
        final M model = findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        return remove(model);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) throws AbstractEntityNotFoundException {
        final M model = findOneByIndex(userId, index);
        if (model == null) throw new EntityNotFoundException();
        return remove(model);
    }

}
